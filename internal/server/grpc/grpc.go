package grpc

import (
	"context"
	"github.com/ashok/m-game-engine/internal/server/logic"
	pbgameengine "github.com/ashok/m-apis/m-game-engine/v1"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"net"
	"google.golang.org/grpc"
)

//Grpc struct 
type Grpc struct {
	address string
	srv *grpc.Server
}

//NewServer returns a connection
func NewServer(address string) *Grpc {
	return &Grpc{
		address: address,

	}
}

// GetSize function helps to get size
func (g *Grpc) GetSize(ctx context.Context, in *pbgameengine.GetSizeRequest) (*pbgameengine.GetSizeResponse, error) {
	log.Info().Msg("GetSize in gameengine called")
	return &pbgameengine.GetSizeResponse{
		Size: logic.GetSize(),
	}, nil
}

// SetScore helps to set the score
func (g *Grpc) SetScore(ctx context.Context, in *pbgameengine.SetScoreRequest) (*pbgameengine.SetScoreResponse, error) {
	log.Info().Msg("SetScore in gameengine called")
	set := logic.SetScore(in.Score)
	return &pbgameengine.SetScoreResponse{
		Set: set,
	}, nil
}

// ListenAndServe helps to listen and serve
func (g *Grpc) ListenAndServe() error {
	lis, err := net.Listen("tcp", g.address)
	if err != nil {
		return errors.Wrap(err, "Failed to open the tcp port")
	}

	serverOpts := []grpc.ServerOption{}

	g.srv = grpc.NewServer(serverOpts...)

	pbgameengine.RegisterGameEngineServer(g.srv,g)

	log.Info().Str("address", g.address).Msg("starting gRPC server for m-game-engine microservice")
	
	err = g.srv.Serve(lis)
	if err != nil {
		return errors.Wrap(err, "Failed to start gRPC server for m-game-engine microservice")
	}
	return nil
}


